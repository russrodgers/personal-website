import React, { Component } from 'react';
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton/IconButton";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import Divider from "@material-ui/core/Divider/Divider";
import List from "@material-ui/core/List/List";
import ListItem from "@material-ui/core/ListItem/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import Icon from "@material-ui/core/Icon/Icon";
import FingerprintIcon from "@material-ui/icons/Fingerprint";
import WorkerIcon from "mdi-material-ui/Worker";
import SchoolIcon from "mdi-material-ui/School";
import Drawer from "@material-ui/core/Drawer/Drawer";
import { DRAWER_WIDTH } from "../constants";


const styles = theme => ({
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  icon: {
    color: theme.palette.primary
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: DRAWER_WIDTH,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9,
    },
  },
});

class Navigation extends Component {
  render() {
    const { classes, open, handleDrawerClose } = this.props;

    return (
      <Drawer
        variant="permanent"
        classes={{
          paper: classNames(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
        <div className={classes.toolbarIcon}>
          {open &&
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon/>
          </IconButton>
          }
        </div>
        <Divider/>
        <List>
          <ListItem button onClick={() => console.log("clicked:", 0)}>
            <ListItemIcon>
              <FingerprintIcon color="primary"/>
            </ListItemIcon>
            <ListItemText>Bio</ListItemText>
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <SchoolIcon color='secondary'/>
            </ListItemIcon>
            <ListItemText>Education</ListItemText>
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <WorkerIcon color='primary' />
            </ListItemIcon>
            <ListItemText>Experience</ListItemText>
          </ListItem>
        </List>
        <Divider/>
        <List>{}</List>
      </Drawer>
    );
  }
}

Navigation.propTypes = {
  classes: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired,
  handleDrawerClose: PropTypes.func.isRequired,
};

export default withStyles(styles)(Navigation);