import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core";
import { withStyles } from '@material-ui/core/styles';
import teal from "@material-ui/core/colors/teal";
import deepOrange from "@material-ui/core/colors/deepOrange";
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Button from "@material-ui/core/Button";
import MenuIcon from "@material-ui/icons/Menu";
import Header from "./Header";

import Navigation from "./Navigation";

const theme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: teal,
    secondary: deepOrange,
  }
});

const styles = theme => ({
  root: {
    display: 'flex',
  },
  appBarSpacer: theme.mixins.toolbar,
  header: {
    height: '100vh',
    backgroundImage: `linear-gradient(
      to right bottom,
      rgb(0, 114, 255, 0.3),
      rgba(40,180,131,0.3)),
      url(../../img/snoflake.jpg)`,
    backgroundSize: 'cover',
    backgroundPosition: 'top',
    position: 'relative',
    opacity: 0.75,
    zIndex: -1,
  },
  // url(../../img/snoflake.jpg)`,
  content: {
    flexGrow: 1,
    // padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },
  chartContainer: {
    marginLeft: -22,
  },
  tableContainer: {
    height: 320,
  },
  icon: {
    margin: theme.spacing.unit * 2
  },
  fab: {
    position: 'fixed',
    left: theme.spacing.unit * 12,
    top: theme.spacing.unit * 2,
  }
});

class App extends Component {
  state = {
    open: false,
  };

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;
    const { open } = this.state;

    return (
      <MuiThemeProvider theme={theme}>
      <Fragment>
        <CssBaseline/>

        <div className={classes.root}>
          {/*<Header*/}
          {/*open={open}*/}
          {/*handleDrawerOpen={this.handleDrawerOpen}*/}
          {/*/>*/}
          <Navigation open={open} handleDrawerClose={this.handleDrawerClose}/>

          <main className={classes.content}>
            <Button
              variant="fab"
              onClick={this.handleDrawerOpen}
              className={classes.fab}
              color='primary'
            >
              <MenuIcon/>
            </Button>
            <div className={classes.header}>
              <Typography component='h1' variant='h1'>Russ Rodgers</Typography>
            </div>
            {/*<div className={classes.appBarSpacer} />*/}
            <Typography variant="display1" gutterBottom component="h2">
              Orders
            </Typography>
            <Typography component="div" className={classes.chartContainer}>
            </Typography>
            <Typography variant="display1" gutterBottom component="h2">
              Products
            </Typography>
            <div className={classes.tableContainer}>
            </div>
          </main>
        </div>
      </Fragment>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);
